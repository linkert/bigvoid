# Big Void pywal template svg :)

Put the svg in your pywal template folder, set a script to do something like this:

    magick $HOME/.cache/wal/bigvoid.svg $HOME/Pictures/Wallpapers/bigvoid.png
    feh --bg-fill $HOME/Pictures/Wallpapers/bigvoid.png

